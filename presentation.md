---
theme: night
transition: slide
logoImg: ""
slideNumber: true
title: "Genetic Evaluation in Adults"
---

<!-- .slide: data-background="white" -->

<small style="color: white;background-color: gray;">**BANGALORE MEDICAL CONGRESS, 2022** @ **16 oct 2022**</small>

### genetic evaluation in adults in day-to-day medicine: when & where

<hr>

#### Col Suman Kumar Pramanik {style="color: darkblue;"}

#### Sr Adv (Med) & Clinical Hematologist {style="color:gray"}

#### Command Hospital (Eastern Command), Kolkata {style="color:gray"}

<hr>

---

<!-- .slide: data-background="white" -->
# genetics: basics

--

## definitions

- **Genotype:** DNA blueprint

- **Phenotype:** Clinical manifestation {.fragment .highlight-red}

- **Germline:** Oocytes and sperms. Can pass genetic materials from parents to children

- **Somatic:** All other cells. Cannot pass genetic materials from parents to children.

- **Chimera:** Collection of cells with differing DNA materials. We are all chimera.

--

Proteins are the effectors of cellular functions {style="color: yellow"}

**Interact** with:

- other proteins
- RNA/DNA

--

## formation of proteins

**central dogma of genetics**

DNA $\rightarrow$ RNA $\rightarrow$ Peptide {style="color:yellow; background-color:darkgreen;"}

- Transcription & translation are **linear processes**

- Regulations are highly complex and **non-linear**

**Genes are hard-coded** and same for all cells at all times but rate of transcription **(gene expression) are highly heterogenous.**

--

## penetrance

Genotype $\rightarrow$ Phenotype is highly variable

The probability of expression of a particular phenotype given a genotype is **penetrance**

---

<!-- .slide: data-background="white" -->
# scope

Testing of DNA, not of gene expression or protein

--

## variants

#### as per ACMG and AMP

- **Pathogenic** {style="color:red"}

- **Likely Pathogenic** {style="color:red"}

- Variant of uncertain (or unknown) significance (VUS)

- Likely benign

- Benign

--

Indicate the degree of confidence that a variant is associated with the disease phenotype, but they do not specify the **likelihood of developing disease**.

---

<!-- .slide: data-background="white" -->
# purpose

Determine **likelihood** that the carrier of the variant

- **has or will develop** a certain condition or disease phenotype,

- predict **which therapies will be more effective** for treatment

--

## disease likelihood depends on ...

- **Accuracy of testing:** Variation in consistency of genetic testing in different laboratories.

    - Not certified by CLIA. {style="color:yellow"}

- **Pathogenicity interpretation:** Different laboratories and over time may produce different interpretations of pathogenicity of a given variant.

--

- **Inheritance pattern:** Monogenic with mendelian inheritance pattern vs multi-genic inheritance pattern.

- **Penetrance:** Highly penetrant conditions (Huntington disease, HTT gene trinucleotide expansion) are invariably present in individuals with disease phenotype.

---

<!-- .slide: data-background="white" -->
## methodologies

--

<!-- .slide: data-background="white" -->
### extent of dna analysis

<br>

<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="801px" viewBox="-0.5 -0.5 801 432" content="&lt;mxfile&gt;&lt;diagram id=&quot;Jy2rnTki9miL87HrEeve&quot; name=&quot;Page-1&quot;&gt;&lt;mxGraphModel dx=&quot;673&quot; dy=&quot;618&quot; grid=&quot;1&quot; gridSize=&quot;10&quot; guides=&quot;1&quot; tooltips=&quot;1&quot; connect=&quot;1&quot; arrows=&quot;1&quot; fold=&quot;1&quot; page=&quot;1&quot; pageScale=&quot;1&quot; pageWidth=&quot;850&quot; pageHeight=&quot;1100&quot; math=&quot;0&quot; shadow=&quot;0&quot;&gt;&lt;root&gt;&lt;mxCell id=&quot;0&quot;/&gt;&lt;mxCell id=&quot;1&quot; parent=&quot;0&quot;/&gt;&lt;mxCell id=&quot;14&quot; value=&quot;&quot; style=&quot;edgeStyle=none;html=1;fontFamily=Helvetica;fontSize=24;&quot; parent=&quot;1&quot; source=&quot;2&quot; target=&quot;13&quot; edge=&quot;1&quot;&gt;&lt;mxGeometry relative=&quot;1&quot; as=&quot;geometry&quot;/&gt;&lt;/mxCell&gt;&lt;mxCell id=&quot;18&quot; value=&quot;&quot; style=&quot;edgeStyle=none;html=1;fontFamily=Helvetica;fontSize=24;&quot; parent=&quot;1&quot; source=&quot;2&quot; target=&quot;17&quot; edge=&quot;1&quot;&gt;&lt;mxGeometry relative=&quot;1&quot; as=&quot;geometry&quot;/&gt;&lt;/mxCell&gt;&lt;mxCell id=&quot;2&quot; value=&quot;&amp;lt;font style=&amp;quot;font-size: 20px;&amp;quot;&amp;gt;&amp;lt;b style=&amp;quot;font-size: 20px;&amp;quot;&amp;gt;Types of Genetic Testing&amp;lt;/b&amp;gt;&amp;lt;/font&amp;gt;&quot; style=&quot;rounded=1;whiteSpace=wrap;html=1;fontSize=20;&quot; parent=&quot;1&quot; vertex=&quot;1&quot;&gt;&lt;mxGeometry x=&quot;442&quot; y=&quot;19&quot; width=&quot;187&quot; height=&quot;60&quot; as=&quot;geometry&quot;/&gt;&lt;/mxCell&gt;&lt;mxCell id=&quot;20&quot; value=&quot;&quot; style=&quot;edgeStyle=none;html=1;fontFamily=Helvetica;fontSize=24;&quot; parent=&quot;1&quot; source=&quot;13&quot; target=&quot;19&quot; edge=&quot;1&quot;&gt;&lt;mxGeometry relative=&quot;1&quot; as=&quot;geometry&quot;/&gt;&lt;/mxCell&gt;&lt;mxCell id=&quot;22&quot; value=&quot;&quot; style=&quot;edgeStyle=none;html=1;fontFamily=Helvetica;fontSize=24;&quot; parent=&quot;1&quot; source=&quot;13&quot; target=&quot;21&quot; edge=&quot;1&quot;&gt;&lt;mxGeometry relative=&quot;1&quot; as=&quot;geometry&quot;/&gt;&lt;/mxCell&gt;&lt;mxCell id=&quot;23&quot; value=&quot;&quot; style=&quot;edgeStyle=none;html=1;fontFamily=Helvetica;fontSize=24;&quot; parent=&quot;1&quot; source=&quot;13&quot; target=&quot;21&quot; edge=&quot;1&quot;&gt;&lt;mxGeometry relative=&quot;1&quot; as=&quot;geometry&quot;/&gt;&lt;/mxCell&gt;&lt;mxCell id=&quot;25&quot; value=&quot;&quot; style=&quot;edgeStyle=none;html=1;fontFamily=Helvetica;fontSize=24;&quot; parent=&quot;1&quot; source=&quot;13&quot; target=&quot;24&quot; edge=&quot;1&quot;&gt;&lt;mxGeometry relative=&quot;1&quot; as=&quot;geometry&quot;/&gt;&lt;/mxCell&gt;&lt;mxCell id=&quot;13&quot; value=&quot;GENE&quot; style=&quot;whiteSpace=wrap;html=1;rounded=1;fontSize=18;&quot; parent=&quot;1&quot; vertex=&quot;1&quot;&gt;&lt;mxGeometry x=&quot;290&quot; y=&quot;159&quot; width=&quot;120&quot; height=&quot;60&quot; as=&quot;geometry&quot;/&gt;&lt;/mxCell&gt;&lt;mxCell id=&quot;17&quot; value=&quot;CHROMOSOME&quot; style=&quot;whiteSpace=wrap;html=1;rounded=1;fontSize=18;&quot; parent=&quot;1&quot; vertex=&quot;1&quot;&gt;&lt;mxGeometry x=&quot;650&quot; y=&quot;159&quot; width=&quot;160&quot; height=&quot;60&quot; as=&quot;geometry&quot;/&gt;&lt;/mxCell&gt;&lt;mxCell id=&quot;27&quot; value=&quot;&quot; style=&quot;edgeStyle=none;html=1;fontFamily=Helvetica;fontSize=24;&quot; parent=&quot;1&quot; source=&quot;19&quot; target=&quot;26&quot; edge=&quot;1&quot;&gt;&lt;mxGeometry relative=&quot;1&quot; as=&quot;geometry&quot;/&gt;&lt;/mxCell&gt;&lt;mxCell id=&quot;29&quot; value=&quot;&quot; style=&quot;edgeStyle=none;html=1;fontFamily=Helvetica;fontSize=24;&quot; parent=&quot;1&quot; source=&quot;19&quot; target=&quot;28&quot; edge=&quot;1&quot;&gt;&lt;mxGeometry relative=&quot;1&quot; as=&quot;geometry&quot;/&gt;&lt;/mxCell&gt;&lt;mxCell id=&quot;19&quot; value=&quot;Single Gene&quot; style=&quot;whiteSpace=wrap;html=1;rounded=1;fontSize=18;&quot; parent=&quot;1&quot; vertex=&quot;1&quot;&gt;&lt;mxGeometry x=&quot;120&quot; y=&quot;260&quot; width=&quot;120&quot; height=&quot;60&quot; as=&quot;geometry&quot;/&gt;&lt;/mxCell&gt;&lt;mxCell id=&quot;21&quot; value=&quot;Multiple Genes&quot; style=&quot;whiteSpace=wrap;html=1;rounded=1;fontSize=18;&quot; parent=&quot;1&quot; vertex=&quot;1&quot;&gt;&lt;mxGeometry x=&quot;290&quot; y=&quot;260&quot; width=&quot;120&quot; height=&quot;60&quot; as=&quot;geometry&quot;/&gt;&lt;/mxCell&gt;&lt;mxCell id=&quot;24&quot; value=&quot;Whole Genome&quot; style=&quot;whiteSpace=wrap;html=1;rounded=1;fontSize=18;&quot; parent=&quot;1&quot; vertex=&quot;1&quot;&gt;&lt;mxGeometry x=&quot;460&quot; y=&quot;260&quot; width=&quot;120&quot; height=&quot;60&quot; as=&quot;geometry&quot;/&gt;&lt;/mxCell&gt;&lt;mxCell id=&quot;26&quot; value=&quot;Single Variant&quot; style=&quot;ellipse;whiteSpace=wrap;html=1;rounded=1;fontSize=18;&quot; parent=&quot;1&quot; vertex=&quot;1&quot;&gt;&lt;mxGeometry x=&quot;10&quot; y=&quot;380&quot; width=&quot;140&quot; height=&quot;70&quot; as=&quot;geometry&quot;/&gt;&lt;/mxCell&gt;&lt;mxCell id=&quot;28&quot; value=&quot;Multiple Variants&quot; style=&quot;ellipse;whiteSpace=wrap;html=1;rounded=1;fontSize=18;&quot; parent=&quot;1&quot; vertex=&quot;1&quot;&gt;&lt;mxGeometry x=&quot;220&quot; y=&quot;380&quot; width=&quot;140&quot; height=&quot;70&quot; as=&quot;geometry&quot;/&gt;&lt;/mxCell&gt;&lt;/root&gt;&lt;/mxGraphModel&gt;&lt;/diagram&gt;&lt;/mxfile&gt;" onclick="(function(svg){var src=window.event.target||window.event.srcElement;while (src!=null&amp;&amp;src.nodeName.toLowerCase()!='a'){src=src.parentNode;}if(src==null){if(svg.wnd!=null&amp;&amp;!svg.wnd.closed){svg.wnd.focus();}else{var r=function(evt){if(evt.data=='ready'&amp;&amp;evt.source==svg.wnd){svg.wnd.postMessage(decodeURIComponent(svg.getAttribute('content')),'*');window.removeEventListener('message',r);}};window.addEventListener('message',r);svg.wnd=window.open('https://viewer.diagrams.net/?client=1&amp;page=0&amp;edit=_blank');}}})(this);" style="cursor:pointer;max-width:100%;max-height:432px;"><defs/><g><path d="M 485.75 60 L 384.83 136.16" fill="none" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="stroke"/><path d="M 380.64 139.33 L 384.12 132.32 L 384.83 136.16 L 388.34 137.9 Z" fill="rgb(0, 0, 0)" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="all"/><path d="M 567.18 60 L 673.15 136.28" fill="none" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="stroke"/><path d="M 677.41 139.35 L 669.69 138.1 L 673.15 136.28 L 673.78 132.42 Z" fill="rgb(0, 0, 0)" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="all"/><rect x="432" y="0" width="187" height="60" rx="9" ry="9" fill="rgb(255, 255, 255)" stroke="rgb(0, 0, 0)" pointer-events="all"/><g transform="translate(-0.5 -0.5)"><switch><foreignObject pointer-events="none" width="100%" height="100%" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow: visible; text-align: left;"><div xmlns="http://www.w3.org/1999/xhtml" style="display: flex; align-items: unsafe center; justify-content: unsafe center; width: 185px; height: 1px; padding-top: 30px; margin-left: 433px;"><div data-drawio-colors="color: rgb(0, 0, 0); " style="box-sizing: border-box; font-size: 0px; text-align: center;"><div style="display: inline-block; font-size: 20px; font-family: Helvetica; color: rgb(0, 0, 0); line-height: 1.2; pointer-events: all; white-space: normal; overflow-wrap: normal;"><font style="font-size: 20px"><b style="font-size: 20px">Types of Genetic Testing</b></font></div></div></div></foreignObject><text x="526" y="36" fill="rgb(0, 0, 0)" font-family="Helvetica" font-size="20px" text-anchor="middle">Types of Genetic Te...</text></switch></g><path d="M 289.5 200 L 225.97 237.75" fill="none" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="stroke"/><path d="M 221.46 240.43 L 225.69 233.84 L 225.97 237.75 L 229.26 239.86 Z" fill="rgb(0, 0, 0)" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="all"/><path d="M 340 200 L 340 234.63" fill="none" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="stroke"/><path d="M 340 239.88 L 336.5 232.88 L 340 234.63 L 343.5 232.88 Z" fill="rgb(0, 0, 0)" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="all"/><path d="M 340 200 L 340 234.63" fill="none" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="stroke"/><path d="M 340 239.88 L 336.5 232.88 L 340 234.63 L 343.5 232.88 Z" fill="rgb(0, 0, 0)" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="all"/><path d="M 390.5 200 L 454.03 237.75" fill="none" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="stroke"/><path d="M 458.54 240.43 L 450.74 239.86 L 454.03 237.75 L 454.31 233.84 Z" fill="rgb(0, 0, 0)" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="all"/><rect x="280" y="140" width="120" height="60" rx="9" ry="9" fill="rgb(255, 255, 255)" stroke="rgb(0, 0, 0)" pointer-events="all"/><g transform="translate(-0.5 -0.5)"><switch><foreignObject pointer-events="none" width="100%" height="100%" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow: visible; text-align: left;"><div xmlns="http://www.w3.org/1999/xhtml" style="display: flex; align-items: unsafe center; justify-content: unsafe center; width: 118px; height: 1px; padding-top: 170px; margin-left: 281px;"><div data-drawio-colors="color: rgb(0, 0, 0); " style="box-sizing: border-box; font-size: 0px; text-align: center;"><div style="display: inline-block; font-size: 18px; font-family: Helvetica; color: rgb(0, 0, 0); line-height: 1.2; pointer-events: all; white-space: normal; overflow-wrap: normal;">GENE</div></div></div></foreignObject><text x="340" y="175" fill="rgb(0, 0, 0)" font-family="Helvetica" font-size="18px" text-anchor="middle">GENE</text></switch></g><rect x="640" y="140" width="160" height="60" rx="9" ry="9" fill="rgb(255, 255, 255)" stroke="rgb(0, 0, 0)" pointer-events="all"/><g transform="translate(-0.5 -0.5)"><switch><foreignObject pointer-events="none" width="100%" height="100%" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow: visible; text-align: left;"><div xmlns="http://www.w3.org/1999/xhtml" style="display: flex; align-items: unsafe center; justify-content: unsafe center; width: 158px; height: 1px; padding-top: 170px; margin-left: 641px;"><div data-drawio-colors="color: rgb(0, 0, 0); " style="box-sizing: border-box; font-size: 0px; text-align: center;"><div style="display: inline-block; font-size: 18px; font-family: Helvetica; color: rgb(0, 0, 0); line-height: 1.2; pointer-events: all; white-space: normal; overflow-wrap: normal;">CHROMOSOME</div></div></div></foreignObject><text x="720" y="175" fill="rgb(0, 0, 0)" font-family="Helvetica" font-size="18px" text-anchor="middle">CHROMOSOME</text></switch></g><path d="M 146 301 L 99.98 358.53" fill="none" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="stroke"/><path d="M 96.7 362.63 L 98.34 354.98 L 99.98 358.53 L 103.8 359.35 Z" fill="rgb(0, 0, 0)" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="all"/><path d="M 196.4 301 L 247.6 359.18" fill="none" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="stroke"/><path d="M 251.07 363.12 L 243.82 360.18 L 247.6 359.18 L 249.07 355.56 Z" fill="rgb(0, 0, 0)" stroke="rgb(0, 0, 0)" stroke-miterlimit="10" pointer-events="all"/><rect x="110" y="241" width="120" height="60" rx="9" ry="9" fill="rgb(255, 255, 255)" stroke="rgb(0, 0, 0)" pointer-events="all"/><g transform="translate(-0.5 -0.5)"><switch><foreignObject pointer-events="none" width="100%" height="100%" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow: visible; text-align: left;"><div xmlns="http://www.w3.org/1999/xhtml" style="display: flex; align-items: unsafe center; justify-content: unsafe center; width: 118px; height: 1px; padding-top: 271px; margin-left: 111px;"><div data-drawio-colors="color: rgb(0, 0, 0); " style="box-sizing: border-box; font-size: 0px; text-align: center;"><div style="display: inline-block; font-size: 18px; font-family: Helvetica; color: rgb(0, 0, 0); line-height: 1.2; pointer-events: all; white-space: normal; overflow-wrap: normal;">Single Gene</div></div></div></foreignObject><text x="170" y="276" fill="rgb(0, 0, 0)" font-family="Helvetica" font-size="18px" text-anchor="middle">Single Gene</text></switch></g><rect x="280" y="241" width="120" height="60" rx="9" ry="9" fill="rgb(255, 255, 255)" stroke="rgb(0, 0, 0)" pointer-events="all"/><g transform="translate(-0.5 -0.5)"><switch><foreignObject pointer-events="none" width="100%" height="100%" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow: visible; text-align: left;"><div xmlns="http://www.w3.org/1999/xhtml" style="display: flex; align-items: unsafe center; justify-content: unsafe center; width: 118px; height: 1px; padding-top: 271px; margin-left: 281px;"><div data-drawio-colors="color: rgb(0, 0, 0); " style="box-sizing: border-box; font-size: 0px; text-align: center;"><div style="display: inline-block; font-size: 18px; font-family: Helvetica; color: rgb(0, 0, 0); line-height: 1.2; pointer-events: all; white-space: normal; overflow-wrap: normal;">Multiple Genes</div></div></div></foreignObject><text x="340" y="276" fill="rgb(0, 0, 0)" font-family="Helvetica" font-size="18px" text-anchor="middle">Multiple Genes</text></switch></g><rect x="450" y="241" width="120" height="60" rx="9" ry="9" fill="rgb(255, 255, 255)" stroke="rgb(0, 0, 0)" pointer-events="all"/><g transform="translate(-0.5 -0.5)"><switch><foreignObject pointer-events="none" width="100%" height="100%" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow: visible; text-align: left;"><div xmlns="http://www.w3.org/1999/xhtml" style="display: flex; align-items: unsafe center; justify-content: unsafe center; width: 118px; height: 1px; padding-top: 271px; margin-left: 451px;"><div data-drawio-colors="color: rgb(0, 0, 0); " style="box-sizing: border-box; font-size: 0px; text-align: center;"><div style="display: inline-block; font-size: 18px; font-family: Helvetica; color: rgb(0, 0, 0); line-height: 1.2; pointer-events: all; white-space: normal; overflow-wrap: normal;">Whole Genome</div></div></div></foreignObject><text x="510" y="276" fill="rgb(0, 0, 0)" font-family="Helvetica" font-size="18px" text-anchor="middle">Whole Genome</text></switch></g><ellipse cx="70" cy="396" rx="70" ry="35" fill="rgb(255, 255, 255)" stroke="rgb(0, 0, 0)" pointer-events="all"/><g transform="translate(-0.5 -0.5)"><switch><foreignObject pointer-events="none" width="100%" height="100%" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow: visible; text-align: left;"><div xmlns="http://www.w3.org/1999/xhtml" style="display: flex; align-items: unsafe center; justify-content: unsafe center; width: 138px; height: 1px; padding-top: 396px; margin-left: 1px;"><div data-drawio-colors="color: rgb(0, 0, 0); " style="box-sizing: border-box; font-size: 0px; text-align: center;"><div style="display: inline-block; font-size: 18px; font-family: Helvetica; color: rgb(0, 0, 0); line-height: 1.2; pointer-events: all; white-space: normal; overflow-wrap: normal;">Single Variant</div></div></div></foreignObject><text x="70" y="401" fill="rgb(0, 0, 0)" font-family="Helvetica" font-size="18px" text-anchor="middle">Single Variant</text></switch></g><ellipse cx="280" cy="396" rx="70" ry="35" fill="rgb(255, 255, 255)" stroke="rgb(0, 0, 0)" pointer-events="all"/><g transform="translate(-0.5 -0.5)"><switch><foreignObject pointer-events="none" width="100%" height="100%" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow: visible; text-align: left;"><div xmlns="http://www.w3.org/1999/xhtml" style="display: flex; align-items: unsafe center; justify-content: unsafe center; width: 138px; height: 1px; padding-top: 396px; margin-left: 211px;"><div data-drawio-colors="color: rgb(0, 0, 0); " style="box-sizing: border-box; font-size: 0px; text-align: center;"><div style="display: inline-block; font-size: 18px; font-family: Helvetica; color: rgb(0, 0, 0); line-height: 1.2; pointer-events: all; white-space: normal; overflow-wrap: normal;">Multiple Variants</div></div></div></foreignObject><text x="280" y="401" fill="rgb(0, 0, 0)" font-family="Helvetica" font-size="18px" text-anchor="middle">Multiple Variants</text></switch></g></g><switch><g requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility"/><a transform="translate(0,-5)" xlink:href="https://www.diagrams.net/doc/faq/svg-export-text-problems" target="_blank"><text text-anchor="middle" font-size="10px" x="50%" y="100%">Viewer does not support full SVG 1.1</text></a></switch></svg>

--

### selected variants vs entire gene

--

### 1. selected variants

1. Factor V Leiden in **thrombophilia workup**.
2. p.Cys282Tyr (p.C282Y) variant in HFE associated with most cases of **hereditary hemochromatosis**.
3. Genotyping of individual variants for **at-risk relatives** of individuals confirmed to carry a pathogenic variant in a specific gene. Eg., BRCA 1 variant

--

### 2. entire gene

- In many genetic disorders, disease gene is sensitive to variation at many locations, with a large number of pathogenic variants. (BRCA 1, BRCA 2 in breast and ovarian cancers, CFTR gene in cystic fibrosis).

- Single nucleotide genotyping panels, sequencing, copy number variations.

--

### 3. entire chromosome

- Chromosome aneuploidy (Down Syndrome, trisomy 21)

-  Segmental chromosomal gains or losses: CNVs, unbalanced chromosome translocations, or other structural cytogenetic rearrangements.

- High resolution karyotyping, FISH, array CGH, SNP array

--

### one gene vs many genes

--

### 1. single gene

When a single gene is implicated in disease causation, testing that gene alone is appropriate.

- Cystic fibrosis: CFTR gene

- Hemophilia B: F9 gene

- Adult onset hereditary hemochromatosis: HFE gene

--

### 2. panel of selected genes

Some disorders are genetically heterogeneous, caused by pathogenic variants in one of several genes.

- Hereditary cancer syndromes

- Hereditary platelet function disorders

--

### 3. all genes

If the clinical presentation does not suggest a particular gene or group of genes, an individual may undergo sequencing of all of the known genes.

- WES and WGS

- Standard tools for gene discovery studies

- Clinical utility limited by availability of experts who can interpret such sequence data {.fragment .highlight-red}

---

<!-- .slide: data-background="white" -->
# testing in adults

**predict disease risk** {style="color:red"}

vs

**establish/confirm diagnosis** {style="color:blue"}

--

## 1. predictive testing (doubtful efficacy at present)

- Risk assessment for unaffected individuals who are at risk for developing conditions with heritable component

- Value of test influenced by **disease penetrance**{.fragment .highlight-green}, availability of **effective preventive strategies**{.fragment .highlight-red} and **early treatment strategies**{.fragment .highlight-red}

--

- Heritable cancer syndromes: increased screening, prophylactic surgery

- Genetic testing directly to healthy people in general population: DTC testing {style="color:red"}

--

## 2. diagnostic testing (more certain efficacy)

- Make or confirm clinical diagnosis of a disease

- Hereditary cancer syndromes: useful tool for identifying most appropriate treatment strategy

--

## 3. carrier testing

- Determine if an individual of reproductive age is heterozygous for a disease variant associated with autosomal recessive or X linked disorders

- Families with family history of Hemophilia A

--

## 4. pharmacogenetic testing

- Guide drug dosing or drug avoidance in individuals with variants that affect drug metabolism or toxicities

- Warfarin resistance (VKORC mutations), 6 MP toxicity (TPMT mutations)

---

<!-- .slide: data-background="white" -->
## questions to consider before testing

--

## 1. is testing indicated?

- Clinical utility depends on the impact of the results on clinical care {style="color:yellow"}

- Huntington disease:

    - Genetic test has high clinical validity (predictive value). <span class="fragment highlight-red">Limited clinical utility</span> for the patient (limited treatment options)

    - <span class="fragment highlight-green">Carrier detection in the family members important</span> to make reproductive plans and addressing various future planning

--

> In other cases, the information from genetic testing raises more questions for the patient than it answers, sometimes at considerable expense and with the potential for harm (eg, from invasive diagnostic procedures).

--

## 2. at what age should the testing be performed?

In general, testing in childhood is appropriate for diseases that manifest in childhood, and deferral to adulthood (or a few years before disease manifestations are expected) is reasonable for adult-onset conditions.

--

## 3. which test is the best?

--

#### single gene or single variant testing

- Single gene disorder is suspected

- A familial disease variant has been identified (Transthyretin amyloidosis)

--

#### multi-gene panels

- Variants in several possible genes may be relevant

- Cancer diagnosis, staging, prognosis and treatment (breast cancer, acute leukemia prognosis, hereditary gastrointestinal cancers, familial acute leukemias)

- Inherited bone marrow failure syndromes

- Preconception testing

- Gene panels for heterogenous conditions like hearing loss

--

#### wgs

- Reserved for complex disorders for which clear panel of implicated genes are not available

---

<!-- .slide: data-background="white" -->
# practical issues

--

## which relatives should be tested?

- Initial testing to be performed on affected individual

- First degree relatives to be tested for specific variant

- Cascade testing on further order relatives

--

## who should order the test?

- Person ordering investigations should be able to 

    - determine appropriate test and methodology
    - address the logistic issues of test ordering
    - prepare patients for psychosocial outcomes and implications for family members
    - interpret results
    - incorporate test findings into management

--

## obtaining informed consent

#### IS A MUST {style="color:yellow"}

--

## submitting proper samples

- For germline variants: any tissue OK (blood sample is preferred, sometimes buccal swabs and saliva)

- Germline testing with active hematologic malignancies: Skin, buccal swab

- Somatic  variants: Appropriate tissue (tumor, bone marrow)

--

DNA can be obtained from any nucleated cell type, but **classical cytogenetic analysis requires dividing cells**

---

## to conclude ...

- Genetic testing can uncover a diagnosis to allow tailored interventions to prevent or treat disease

- Thorough understanding of genotype to phenotype is required for interpreting results

- Associated with a number of ethical, psychosocial and legal issues

--

> Detailed counselling of the patient with clear plan of action should be in place for all possible scenarios of the results before asking for the test.

---

<!-- .slide: data-background="white" -->
# Thank You {style="color:#eee;font-style:italic;text-shadow:4px 4px #000;"}