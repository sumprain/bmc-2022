---
title: "Genetic evaluation in adults in day-to-day medicine: when & where?"
author: "Col Suman Kumar Pramanik"
---

## Introduction

Genetic testing can uncover a diagnosis that allows tailored interventions to prevent or treat disease or disease complications. Appropriate use of genetic testing, including understanding the limitations and challenges of available testing approaches, is crucial to the successful use of genetic testing in improving health and quality of life.

This manuscript reviews principles of clinical genetic evaluation in adults, including testing methods, indications of 
testing and practical issues.

## Basics of genetics

The **genotype** can be defined as deoxyribonucleic acid (DNA) blueprint that is associated with the clinical manifestation of a trait, **phenotype** 
or disease. The DNA content of a cell remains hardcoded in the cell and it can only change during the process of cell division through process 
of various forms of mutations.

The origin of all the cells in the human being is the zygote, which is formed from the nuclear materials from mother (oocyte) and father (sperm). 
The oocytes and sperms are called as **germline** cells, which has got the potential to pass on the nuclear material to next generation, rest of the 
cells are known as **somatic**, which do not have the ability to pass on the genetic material to next generation of human (although they can pass 
on the materials to next generation of cells). Each mitosis starting from the zygote stage, to early oligo-cellular (8 - 32 cell stage) and later on 
stages leads to random DNA mutations, which lead the human being as collection of cells with differing DNA materials (**chimera**). Earlier occurring mutation is transmitted to more number of offspring cells (like a mutation occurring in zygote, 1 cell stage, will be found in 
all the cells of human being and a mutation occurring in one of the 2 cells in two cell stage will be found in approximately 50% of cells of human 
being).

As per the current evidence, **proteins are the effectors** for the day to day living of the cells and their proportions and interactions with other 
proteins, RNA and DNA lead to various disease conditions and phenotypes.

### Formation of proteins

As per the central dogma of genetics, DNA are **transcribed** into RNA, which further **translate** into proteins. DNA are organized into **genes**, 
which contain genetic code in a linear arrangement for one peptide chain. Genes contain regulatory regions, coding regions (**exon**) and 
interspersed non-coding regions (**introns**). Although genes are linear collections of DNA base pairs, their regulatory processes are highly non-linear, involving various DNA fragments, RNA fragments, proteins and other epigenetic mechanisms (methylation and histone acytelation). 
So, although genes are same in all the cells of a human being, but their rate of transcription into RNAs (**gene expression**) is 
highly heterogenous with respect to time and cells. Usually RNA have a short half life and RNA concentration in any particular cell is 
highly dynamic and depends of the balance between the formation and destruction of RNAs.

The process of translation of RNA into peptide chains is subjected to lesser amount of regulation, but the maturation of peptide chain by the 
processes of post-translational modification are subjected to fine regulatory steps.

Many peptide chains, which are the product of different genes, combine with each other to form mature and effector proteins.

So, the phenotype (which is the result of action of different proteins) of a human being is the result of highly complex, non-linearly regulated, 
time and cell wise heterogenous processes occurring from the genotype. Chances that a particular genotype will get translated 
into a particular disease phenotype are widely variable. The above mentioned chance is known as **penetrance** of the genotype.

## Scope of genetic evaluation

For this manuscript, genetic evaluation means testing involving genotype (DNA). It does not include testing for proteins and gene expression testing.

After genetic evaluation, we can get **variants**, which are the variations from the reference sequence. The variants are classified into 
standard classes (as defined by American College of Medical Genetics [ACMG] and Genomics and Association of Molecular Pathology [AMP]):

1. Pathogenic
2. Likely pathogenic (at least 90 percent confidence in pathogenicity)
3. Variant of uncertain (or unknown) significance (VUS)
4. Likely benign (at least 90 percent confidence in benign status)
5. Benign

Generally, variants classified as pathogenic or likely pathogenic are considered appropriate for incorporating into clinical care.

These designations indicate the degree of confidence that a variant is associated with the disease phenotype, but they do not 
specify the **likelihood of developing disease**.

## Purpose of genetic evaluation

The purpose of genetic testing is to **determine the likelihood** that an individual has or will develop a certain condition or disease
phenotype, and in some cases, to predict which therapies will be more effective for treatment.

Disease likelihood depends on a number of technical and biologic factors.

1. **Accuracy of testing:** There can be variation in the consistency of genetic test results, especially for testing performed
in a laboratory that has not been certified by the Clinical Laboratory Improvement Amendments (CLIA).

2. **Pathogenicity interpretation:** The pathogenicity of variants identified from genetic testing is determined
using a combination of epidemiologic, clinical, and research data that provide information about the association of the 
variant with disease. Different CLIA-certified or non-certified laboratories may sometimes produce different interpretations 
of the pathogenicity classification of the same genetic variant, and as previously discussed, classification may change over time 
as more information becomes available.

3. **Inheritance pattern:** Some conditions are monogenic with a Mendelian inheritance pattern. Examples include certain cancer syndromes or 
metabolic disorders. Other conditions are multi-genic or multifactorial. For monogenic disorders, the inheritance pattern (autosomal or sex-linked; recessive or dominant) determines whether heterozygosity is likely or sufficient to confer increased risk of disease or whether homozygosity 
or compound heterozygosity is necessary for the disease to manifest. For multifactorial disease, the predictability of a particular variant 
to cause a disease is at the best very nominal.

4. **Penetrance:** Highly penetrant conditions are those in which disease is invariably present in individuals with the
disease genotype. An example of a highly penetrant condition is Huntington disease in individuals who have a pathogenic 
trinucleotide expansion in the HTT gene. Disease variants causing cancer can initially be identified from kindreds with 
high-disease penetrance. In many cases, subsequent studies in the general population reveal a lower penetrance rate for the same variant, like 
with certain cancer genes such as the breast cancer genes BRCA1 and BRCA2.

## Methodologies of genetic testing

### Extent of DNA analysis

The extent of genetic testing can range from analyzing a single variant (such as the variant for factor V Leiden) to the entire
genome. The appropriate test depends on the indication or presenting feature(s), the tests available for the suspected condition(s), 
and the available information regarding the genetic cause(s) of the condition or presenting features.

#### Selected variants vs entire gene

For every gene, it is possible to evaluate selected variants (certain nucleotide substitutions or other changes) or
the entire gene (every coding nucleotide and the nucleotides near the splice sites in the coding region of the gene).

##### Selected Variants

1. Factor V Leiden in thrombophilia workup.
2. p.Cys282Tyr (p.C282Y) variant in HFE associated with most cases of hereditary hemochromatosis.
3. Genotyping of individual variants for at-risk relatives of individuals confirmed to carry a pathogenic variant in a specific gene. Like, 
siblings and offspring of a carrier of a pathogenic BRCA1 variant can be tested for the familial pathogenic variant alone.

##### Entire gene

For many genetic disorders, the disease gene is very sensitive to variation at many locations, with large numbers of pathogenic variants
known to cause disease. Examples include the breast and ovarian cancer genes, BRCA1 and BRCA2, and the cystic fibrosis gene, CFTR.

Various approaches to testing the above are single nucleotide genotyping panels, sequencing and copy number variations.

##### Entire chromosome

Some disorders, such as Down syndrome (trisomy 21), are associated with chromosome aneuploidy. Segmental chromosomal
gains or losses, also resulting from CNVs, unbalanced chromosome translocations, or other structural cytogenetic rearrangements, can also be
associated with genetic conditions. This form of genetic defect occurs at a much larger scale compared to nucleotide substitutions.
Methods such as high-resolution karyotyping, fluorescence in situ hybridization (FISH), array comparative genomic hybridization (array CGH), 
or SNP arrays can be used.

#### One gene vs many genes

##### Single gene

Some disorders are clearly linked to a single gene. Examples include conditions such as cystic fibrosis, which is associated with variants
in the CFTR gene; hemophilia B, caused by variants in the F9 gene; or adult-onset hereditary hemochromatosis, associated with variants 
in the iron regulatory gene HFE.

When a single gene is implicated, testing that gene alone is generally appropriate, with additional genetic testing reserved for 
individuals for whom the initial testing was uninformative.

##### Panel of selected genes

Some disorders are genetically heterogeneous, caused by pathogenic variants in one of several genes. Examples include
hereditary cancer syndromes or hereditary platelet function disorders. In such cases, gene panel testing for the known genes associated with the
disorder is usually considered more efficient and cost-effective.

##### All genes

If the clinical presentation does not suggest a particular gene or group of genes, an individual may undergo sequencing of all of the known
genes as part of whole exome sequencing (WES) or whole genome sequencing (WGS). WES or WGS is now a standard tool for gene discovery studies 
where a large number of patients (or ideally, large kindreds) with similar disease manifestations are available for study. 

Their utility in the clinical setting, however, is largely dependent on the availability of practitioners with expertise in 
interpretation of such sequence data.

## Testing in Adults

Genetic testing for adult-onset conditions can be used to predict disease risk (**presymptomatic testing**) or to establish or confirm a
diagnosis (**diagnostic testing**).

Testing can be conceptualized according to its purpose:

1. **Predictive testing:** Predictive testing is a method of risk assessment for unaffected individuals who are at risk for developing 
conditions with a hereditable component (especially in kindreds of patient). Knowledge of an increased risk for a 
particular disease may lead to interventions to decrease the risk. The value of a test is influenced by the disease penetrance and 
whether there are effective prevention or early treatment strategies to impact its clinical course.

Examples include increased monitoring or prophylactic surgery for variants associated with hereditary cancer syndromes, or additional testing or
interventions for variants associated with hypertrophic cardiomyopathy.

Some companies are marketing genetic testing directly to healthy people in the general population, referred to as direct-to-consumer (DTC) testing.

2. **Diagnostic testing:** Used to make or confirm the clinical diagnosis of a disorder. 
For hereditary cancer syndromes, diagnostic testing may be a useful tool for identifying the most appropriate treatment strategies. 
Examples include more extensive or less extensive cancer surgeries or use of targeted chemotherapy or biologic therapy known to be effective 
or ineffective in tumors with certain genotypes.

3. **Carrier testing:** Used to determine if an individual of reproductive age is heterozygous for a disease variant associated 
with an autosomal recessive disorder or X-linked disorders.

Carrier testing can be performed in families with a family history for a specific genetic condition (like hemophilia A).

4. **Pharmacogenetic testing:** Used to guide drug dosing or drug avoidance in individuals with variants that affect drug metabolism or toxicities. 
The results may have no effect on health, but their identification may be critical for administration of certain drugs.

## Questions to consider before genetic evaluation

### Is the testing indicated?

The **clinical utility** of a genetic test depends on the **impact of the results on clinical care**. As an example, while the genetic test for Huntington disease has high clinical validity (predictive value), the clinical utility is limited because of few treatment or 
intervention options available for those who test positive. However, family members at risk for this disease may choose to undergo genetic 
testing to make reproductive plans or for the psychologic benefit in relieving uncertainty and facilitating future planning. In other
cases, the information from genetic testing raises more questions for the patient than it answers, sometimes at considerable expense and with the potential for harm (eg, from invasive diagnostic procedures).

### At what age should the testing be performed?

In general, testing in childhood is appropriate for diseases that manifest in childhood, and deferral to 
adulthood (or a few years before disease manifestations are expected) is reasonable for adult-onset conditions.
Deferral to adulthood in these cases allows proper informed consent and ensures that the most current 
testing methods and interpretation of the clinical implications are available to the patient's 
current clinician(s).

### Which test is the best?

As [discussed above](#methodologies-of-genetic-testing), testing can analyze different numbers of genes and different numbers of variants within those genes. It is worthwhile considering what extent of testing and what testing method will provide the most useful information with the least cost and burden to the
patient.

1. **Single gene or single variant testing** is appropriate when a single gene disorder is suspected or a 
familial disease variant has been identified and there is value in determining whether an individual 
carries that variant. Transthyretin amyloidosis is an example.

2. **Multi-gene panels** are particularly helpful for settings in which variants in several possible genes may be relevant. Examples include:

    a. Breast Cancer, acute leukemia prognosis, hereditary gastrointestinal cancers, familial acute leukemias, inherited bone 
    marrow failure syndromes

    b. Preconception testing

    c. Gene panels for heterogenous conditions like hearing loss

3. More extensive testing such as **WGS** can generally be reserved for complex disorders for which 
a clear panel of implicated genes is not available.

## Practical Issues

### Which relatives should be tested?

Whenever possible, initial genetic testing should be performed on an affected individual. Once a disease variant has been identified, 
testing of first-degree relatives is technically straightforward, and they are usually tested for the specific variant. 
Cascade testing to the first-degree relatives of individuals who test positive can then be performed. 
Relatives who have inherited the variant may be candidates for enhanced screening or risk reduction strategies, while those who have not 
inherited the alteration can be spared unnecessary procedures.

### Who should order the test?

Genetic evaluation can be performed in the context of a genetic counseling appointment with a genetic counselor or clinical geneticist, 
by a primary care provider, or by a disease specialist. What is most important is that individuals ordering genetic testing are able to 
determine the appropriate test and methodology, address the logistic issues of test ordering, prepare patients for psychosocial outcomes 
and implications for family members, interpret results, and incorporate test findings into management.

### Obtaining informed consent

Informed consent, a tenet of patient-centered medicine, is a foundation for the voluntary nature of genetic testing. It means that the 
patient fully understands and agrees to the procedure.

### Submitting proper samples (source of DNA)

Genetic testing for germline variants can be conducted on virtually any tissue. Most laboratories prefer blood specimens, 
although cheek (buccal) swabs and saliva samples may be an option for certain types of genetic testing. 
Germline testing of individuals with active hematologic malignancies often requires isolating DNA from skin or other tissue samples.
Testing for acquired (somatic) mutations requires the appropriate tissue (eg, tumor, bone marrow). 
DNA can be obtained from essentially any nucleated cell type, whereas classical cytogenetic analysis, such as karyotyping, 
requires dividing cells.

### Where to test?

Some genetic tests for common variants such as factor V Leiden are offered through many laboratories, 
while other genetic tests are more specialized and may only be available from one or two clinical laboratories. 
It is important to select a testing laboratory with well-curated clinical and molecular genetic information on the genes being tested.

## Points to be taken

- Genetic testing can uncover a diagnosis that allows tailored interventions to prevent or treat disease or disease complications. 

- A thorough understanding of the genotype to phenotype is required for a particular disease for interpreting the results obtained from the 
tests.

- The tests are associated with a number of ethical, psychosocial and legal issues.

- Before asking for the tests, detailed counselling of the patient is to be done and a clear plan of action should be in place for all 
possible scenarios of result of the test.

## Suggested reads

1. Handbook of Clinical Adult Genetics and Genomics. DOI: https://doi.org/10.1016/B978-0-12-817344-2.00003-4

2. Assessing Genetic Risks: Implications for Health and Social Policy. http://www.nap.edu/catalog/2057.html
